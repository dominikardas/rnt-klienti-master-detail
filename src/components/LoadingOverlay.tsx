import React from 'react';

class LoadingOverlay extends React.Component {
  render() {
    return (
      <div className="loading-overlay">
        <div className="lds-dual-ring"></div>
      </div>
    );
  }
}

export { LoadingOverlay };
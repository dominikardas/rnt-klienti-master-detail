import React from 'react';

import { Row } from './Row';

import { ListViewColumns } from '../../interfaces/ListView';

interface IProps {

  columns: ListViewColumns[],
  data: Array<any>,
  
  onClickRow?: (id: number) => void,
};


class Rows extends React.Component<IProps> {
  render() {
    return (
      <>
        {
          this.props.data.length > 0
          ?
          <tbody>
            {
              this.props.data.map((row, i) => (
                <Row key={ i } columns={ this.props.columns } row={ row } onClickRow={ this.props.onClickRow } />
              ))
            }
          </tbody>
          :
          <tbody>
            <tr>
              <td colSpan={ this.props.columns.length }>
                <p>
                  Nebyly nalezeny žádné záznamy
                </p>
              </td>
            </tr>
          </tbody>
        }
      </>
    );
  }
}

export { Rows };
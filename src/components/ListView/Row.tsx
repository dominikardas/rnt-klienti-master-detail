import React from 'react';

import { ListViewColumns } from '../../interfaces/ListView';

interface IProps {

  columns: ListViewColumns[],
  row: Array<any>,

  onClickRow?: (id: number) => void,
};


class Row extends React.Component<IProps> {

  onClickRow() {
    if (this.props.onClickRow !== undefined) {
      const entryId = parseInt(this.props.row['id' as any]);
      this.props.onClickRow(entryId);
    }
  }

  render() {
    return (
      <tr onClick={ () => this.onClickRow() }>
        {
          this.props.columns.map((column, i) => {

            let value = '';
            
            // Neobsahuje oddělovač - hodnota je na první úrovni objektu
            if (!column['value'].includes('.')) {
              value = this.props.row[column['value'] as any];
            // Obsahuje oddělovač - hodnota je vnořená na vyšší úrovni
            } else {

              let currColValue: any = null;
              const columns = column['value'].split('.');

              columns.map((el: any) => {
                currColValue = (currColValue === null) ? this.props.row[el] : currColValue[el];
              });

              if (currColValue !== undefined) {
                value = currColValue; 
              }
            }

            return (
              <td key={ i }>
                <p>{ value }</p>
              </td>
            )
          })
        }
      </tr>
    );
  }
}

export { Row };
import React from 'react';

import { Header } from './Header';
import { Rows } from './Rows';

import { ListViewColumns } from '../../interfaces/ListView';

interface IProps {

  columns: ListViewColumns[],
  data: Array<any>,

  lvClass?: string,

  onClickRow?: (id: number) => void,
}


class ListView extends React.Component<IProps> {
  render() {
    return (
      <table className={"list-view " + (this.props.lvClass ?? '') }>
        <Header columns={ this.props.columns } />
        <Rows columns={ this.props.columns } data={ this.props.data } onClickRow={ this.props.onClickRow } />
      </table>
    );
  }
}

export { ListView };
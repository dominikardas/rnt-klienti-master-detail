import React from 'react';

import { ListViewColumns } from '../../interfaces/ListView';

interface IProps {
  columns: ListViewColumns[],
};


class Header extends React.Component<IProps> {
  render() {
    return (
      <thead>
        <tr>
          {
            this.props.columns.map((column, i) => (
              <th key={ i }>
                <p>{ column.heading }</p>
              </th>
            ))
          }
        </tr>
      </thead>
    )
  }
}

export { Header };
import React from 'react';

import './detail.scss';

import { ClientDetail } from '../../interfaces/Clients';
import { LoadingOverlay } from '../LoadingOverlay';

import { pairClientRelationship } from '../../utils/clients';

interface IProps {
  loading: boolean,
  client?: ClientDetail,
};

interface IState {
};


class Detail extends React.Component<IProps, IState> {
  
  showOnMap() {

    if (this.props.client) {

      const mapsUrl = 'https://www.google.cz/maps?q=';
      const query = this.props.client.primaryAddress.address.street + ', ' + this.props.client.primaryAddress.address.city;

      window.open(mapsUrl + query, '_blank');
    }
  }

  renderDetailFlags() {
    
    if (this.props.client === undefined) { return; }

    return (
      <div className="detail__flags">
        {
          this.props.client.role !== ''
          ? <p>{ pairClientRelationship(this.props.client.role) }</p>
          : ''
        }
        {
          (this.props.client.category?.value ?? '') !== ''
          ? <p>Kategorie: { this.props.client.category?.value }</p>
          : ''
        }
      </div>
    );
  }

  renderDetailLogo() {

    if (this.props.client === undefined) { return; }

    return (
      <div className="detail__logo">
        {
          this.props.client.logo.logoB64 !== ''
          ?
          <img
            src={ this.props.client.logo.logoB64 }
            alt={ this.props.client.name }
            className="responsive-img"
          />
          :
          <img
            alt={ this.props.client.name }
            className="responsive-img no-img"
          />
        }
      </div>
    );
  }

  renderDetailInfo() {

    if (this.props.client === undefined) { return; }

    return (
      <div className="detail__info">
        <p><strong>IČO: { this.props.client.regNumber ? this.props.client.regNumber : '-' }</strong></p>
        <div className="info__address">
          <p>{ this.props.client.primaryAddress.address.street ?? '' }</p>
          <p>{ (this.props.client.primaryAddress.address.zipCode ?? '') + ' ' +  (this.props.client.primaryAddress.address.city ?? '') }</p>
          <p>{ this.props.client.primaryAddress.address.country ?? '' }</p>
        </div>
        {
          (
            ((this.props.client.primaryAddress.address.street ?? '') !== '') &&
            ((this.props.client.primaryAddress.address.city ?? '') !== '')
          )
          &&
          <p>
            <button className="link" onClick={ (e) => this.showOnMap() }>Zobrazit na mapě</button>
          </p>
        }
        
      </div>
    );
  }

  renderDetailLogoInfo() {

    if (this.props.client === undefined) { return; }

    return(
      <div className="detail_logo_info">
        { this.renderDetailLogo() }
        { this.renderDetailInfo() }
      </div>
    );
  }

  renderDetail() {
    return (
      <>
        {
          this.props.client !== undefined
          ?
          <div className="clients__detail">

            <h2>{ this.props.client.name }</h2>

            { this.renderDetailFlags() }
            { this.renderDetailLogoInfo() }

            <p>{ this.props.client.notice }</p>

            <p>Vlastník: <strong>{ this.props.client.owner.fullName }</strong></p>

          </div>
          :
          <div className="clients__detail client_not-selected">
            <p>Pro zobrazení detailu klikněte na klienta ze seznamu</p>
          </div>
        }
      </>
    );
  }

  render() {
    return (
      <>
        {
          !this.props.loading
          ?
          this.renderDetail()
          :
          <div className="clients__detail">
            <LoadingOverlay />
          </div>
        }
      </>
    );
  }
}

export { Detail };
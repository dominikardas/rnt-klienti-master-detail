import React from 'react';
import ReactDOM from 'react-dom/client';

import axios from 'axios';

import App from './App';


if (!process.env.REACT_APP_RNT_API_URL)       { throw new Error('Proměnná "REACT_APP_RNT_API_URL" nebyla nastavena!') }
if (!process.env.REACT_APP_RNT_API_USER)      { throw new Error('Proměnná "REACT_APP_RNT_API_USER" nebyla nastavena!') }
if (!process.env.REACT_APP_RNT_API_KEY)       { throw new Error('Proměnná "REACT_APP_RNT_API_KEY" nebyla nastavena!') }
if (!process.env.REACT_APP_RNT_API_INSTANCE)  { throw new Error('Proměnná "REACT_APP_RNT_API_INSTANCE" nebyla nastavena!') }

axios.defaults.baseURL = process.env.REACT_APP_RNT_API_URL;
axios.defaults.headers.common['Content-Type']    = 'application/json';
axios.defaults.headers.common['Authorization']   = 'Basic ' + window.btoa(process.env.REACT_APP_RNT_API_USER + ':' + process.env.REACT_APP_RNT_API_KEY);
axios.defaults.headers.common['X-Instance-Name'] = process.env.REACT_APP_RNT_API_INSTANCE ?? '';


const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
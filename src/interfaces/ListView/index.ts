export interface ListViewColumns {
  heading: string,
  value: string,
}
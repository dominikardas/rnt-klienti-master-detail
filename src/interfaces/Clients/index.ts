export interface Client {
  id: number,
  name: string,
  notice: string,
  state: string,
  role: string,
  rating: string,
  owner: {
    fullName: string,
  },
  regNumber: string,
  contactAddress: {
    address: {
      city: string,
    },
  },
  category?: {
    id?: number,
    value?: string
  },
}

export interface ClientsFilter {
  fulltext: string,
}

export interface ClientDetail {
  id: number,
  name: string,
  notice: string,
  state: string,
  role: string,
  logo: {
    fileName: string,
    id: number,
    logoB64: string
  },
  owner: {
    fullName: string,
  },
  regNumber: string,
  primaryAddress: {
    address: {
      city: string,
      country: string,
      street: string,
      zipCode: string,
    },
  },
  category?: {
    id: number,
    value: string
  },
}
import React from 'react';

import './assets/styles/App.scss';

import { Clients } from './containers/Clients/';


export default class App extends React.Component {
  render() {
    return (
      <div className="container">
        <Clients />
      </div>
    );
  }
}
import React from 'react';

import './clients.scss';

import { ListView } from '../../components/ListView';
import { Detail } from '../../components/Clients/Detail';
import { LoadingOverlay } from '../../components/LoadingOverlay';

import { Client, ClientsFilter, ClientDetail } from '../../interfaces/Clients';
import { ListViewColumns } from '../../interfaces/ListView';

import ClientsService from '../../services/clientsService';

interface IState {

  listLoading: boolean,
  detailLoading: boolean,

  filters: ClientsFilter,

  clients: Client[],
  selectedClient?: ClientDetail,

  lvColumns: ListViewColumns[],
};


class Clients extends React.Component<any, IState> {

  constructor(props: any) {

    super(props);
    
    this.state = {

      listLoading: true,
      detailLoading: false,

      clients: [],

      filters: {
        fulltext: '',
      },

      lvColumns: [
        { heading: 'Název / Jméno',  value: 'name'},
        { heading: 'Stav',           value: 'state'},
        { heading: 'Vztah',          value: 'role'},
        { heading: 'Rating',         value: 'rating'},
        { heading: 'Vlastník',       value: 'owner.fullName'},
        { heading: 'IČO',            value: 'regNumber'},
        { heading: 'Město',          value: 'contactAddress.address.city'},
        { heading: 'Kategorie',      value: 'category.value'},
      ],
    };

    this.handleSearchKeyPress = this.handleSearchKeyPress.bind(this);
    this.setClientDetail      = this.setClientDetail.bind(this);
  }


  async setClients() {
    
    this.setState({ listLoading: true });

    const clients: Client[] = await ClientsService.getClients(this.state.filters);
    this.setState({ clients: clients, listLoading: false });
  }

  async setClientDetail(id: number) {

    this.setState({ detailLoading: true });

    const response = await ClientsService.getClientDetail(id);
    this.setState({ selectedClient: response, detailLoading: false });
  }

  filterActive() {
    const fulltextActive = (this.state.filters.fulltext !== '');
    return fulltextActive;
  }

  async handleSearchKeyPress(e: React.KeyboardEvent<HTMLInputElement>) {
    
    if (e.key === 'Enter') {

      const value = (e.target as HTMLInputElement).value;

      this.setState((state: IState) => {
        return {
          filters: {
            ...state.filters,
            'fulltext': value,
          },
          listLoading: true
        }
        
      }, async () => {
        await this.setClients();
      });
    }
  }

  async componentDidMount() {
    await this.setClients();
  }

  render() {
    return (
      <div className="view-wrapper">
          <div className="view view-clients">
            
              <div className="view-header">
                <h1>Klienti</h1>
                <div>
                  <span className={ `flag flag-filter-active ${ (!this.filterActive() ? "hidden" : "") }` }>
                    Seznam je filtrován
                  </span>
                </div>
              </div>

              <div className="form-field-search">
                <input id="fulltext" name="fulltext" type="text" placeholder="Hledat..." onKeyUp={ this.handleSearchKeyPress } />
              </div>

              {
                !this.state.listLoading 
                ?
                <div className="clients__list_detail">
                  <ListView columns={ this.state.lvColumns } data={ this.state.clients } onClickRow={ this.setClientDetail } lvClass="lv-clients" />
                  <Detail client={ this.state.selectedClient } loading={ this.state.detailLoading } />
                </div>
                :
                <LoadingOverlay />
              }
          </div>
      </div>
    );
  }
}

export { Clients };
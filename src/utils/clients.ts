function pairClientStateName(state: string) {
  switch (state) {
    case 'A_POTENTIAL':     return 'Potencionální';
    case 'B_ACTUAL':        return 'Aktuální';
    case 'C_DEFERRED':      return 'Odložený';
    case 'D_UNATTRACTIVE':  return 'Nezajímavý';
    default: return state;
  }
}

function pairClientRelationship(relationship: string) {
  switch (relationship) {
    case 'A_SUBSCRIBER':  return 'Odběratel';
    case 'B_PARTNER':     return 'Partner';
    case 'C_SUPPLIER':    return 'Dodavatel';
    case 'D_RIVAL':       return 'Konkurent';
    case 'E_OWN':         return 'Vlastní firma';
    default: return relationship;
  }
}

export { pairClientStateName, pairClientRelationship };

function getUrlParams(params: Object) {

  const paramsStr = Object.keys(params).map((key) => {
    return key + '=' + encodeURIComponent((params as any)[key]);
  }).join('&');
  
  return paramsStr;
}

export { getUrlParams };

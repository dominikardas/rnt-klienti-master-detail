import axios from 'axios';

import { Client, ClientDetail } from '../../interfaces/Clients';

import { getUrlParams } from '../../utils/';
import { pairClientStateName, pairClientRelationship } from '../../utils/clients';


class ClientsService {

   /* Získání seznamu klientů z API */
  async getClients(filters?: Object): Promise<Client[]> {

    const urlParams = getUrlParams(filters ?? '');
    const clientsUrl = 'company/?' + urlParams;

    try {
      
      const response = await axios
      .get(clientsUrl);

      const clients: Client[] = [];
      response.data.data.map((client: Client) => {
        clients.push({
          id: client.id,
          name: client.name,
          notice: client.notice,
          state: pairClientStateName(client.state),
          role: pairClientRelationship(client.role),
          rating: client.rating,
          owner: {
            fullName: client.owner.fullName,
          },
          regNumber: client.regNumber,
          contactAddress: {
            address: {
              city: client.contactAddress.address.city,
            },
          },
          category: {
            id: client.category?.id,
            value: client.category?.value
          },
        })
      });
  
      return clients;

    } catch (err) {
      throw new Error('[ClientsService@getClients] Při požadavku na API nastala chyba! ' + err);
    }
  }

  /* Získání dat klienta pro zobrazení detailu */
  async getClientDetail(id: number): Promise<ClientDetail> {
    
    const clientDetailUrl = 'company/' + id + '/';

    try {

      const response = await axios
      .get(clientDetailUrl)
      
      const clientData = response.data.data;
      const clientDetail: ClientDetail = {
        id: clientData.id,
        name: clientData.name,
        notice: clientData.notice,
        state: clientData.state,
        role: clientData.role,
        logo: {
          fileName: clientData.logo?.fileName,
          id: clientData.logo?.id,
          logoB64: '',
        },
        owner: {
          fullName: clientData.owner.fullName,
        },
        regNumber: clientData.regNumber,
        primaryAddress: {
          address: {
            city: clientData.primaryAddress.address.city,
            country: clientData.primaryAddress.address.country,
            street: clientData.primaryAddress.address.street,
            zipCode: clientData.primaryAddress.address.zipCode,
          },
        },
        category: {
          id: clientData.category?.id,
          value: clientData.category?.value,
        },
      };

      if (clientData.logo !== null) {
        const logoB64 = await this.getClientLogoB64(clientData.logo.id)
        clientDetail.logo.logoB64 = logoB64;
      }
      
      return clientDetail;

    } catch (err) {
      throw new Error('[ClientsService@getClientDetail] Při požadavku na API nastala chyba! ' + err);
    }
  }
  
  async getClientLogoB64(imgId: number): Promise<string> {

    const clientLogoUrl = 'image/' + imgId + '/';
    
    try {
      
      const response = await axios
      .get(clientLogoUrl);

      return response.data.imgData;

    } catch (err) {
      throw new Error('[ClientsService@getClientLogoB64] Při požadavku na API nastala chyba! ' + err);
    }
  }
}

export default new ClientsService();